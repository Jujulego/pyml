import cv2
import numpy as np
import os

from datetime import datetime
from pyml.algorithms import FFNN2L


def load(dir):
    names = []
    inputs = []
    outputs = []

    for n in [0, 1, 2]:
        for img in os.listdir(f'{dir}/{n}'):
            if not img.endswith('.png'):
                continue

            gray_img = cv2.imread(f'{dir}/{n}/{img}', cv2.IMREAD_GRAYSCALE)

            names.append(f'{dir}/{n}/{img}')
            inputs.append(gray_img.flatten())
            outputs.append(n)

    return names, inputs, outputs


def output(n):
    o = [0.0, 0.0, 0.0]
    o[n] = 1.0

    return o


def color(p):
    if p > 0.75:
        color = 32  # vert
    elif p < 0.25:
        color = 31  # rouge
    else:
        color = 33  # jaune

    return "\033[{}m{: .2f} %\033[m".format(color, p * 100)


if __name__ == '__main__':
    # load data
    print("Loading ...")
    names, inputs, outputs = load("images")
    outputs = [output(y) for y in outputs]

    # FFNN2L
    ffnn2l = FFNN2L(k=10000, alpha_v=0.5, alpha_w=0.125, epsilon=0.05)

    print("Training ...")
    start = datetime.now()
    ffnn2l.train(inputs, outputs)
    end = datetime.now()

    print(ffnn2l.theta[0])
    print(ffnn2l.theta[1])

    print("Testing ...")
    result = ffnn2l.test(inputs, outputs)
    for n, y, g in zip(names, outputs, result.results):
        print(n, y, color(g[0]), color(g[1]), color(g[2]), sep="\t")

    print("Compute time:", (end - start).total_seconds() * 1000, "ms")
    print("SSE:", result.sse)
    print("MSE:", result.mse)
