from .model import Model


class Linear(Model):
    def modelize(self, x):
        return x

    def plot(self, x, theta):
        return theta[0] + theta[1] * x
