import numpy as np

from .model import Model


class Exponential(Model):
    def modelize(self, x):
        return [np.exp(xn) for xn in x]

    def plot(self, x, theta):
        return theta[0] + theta[1] * np.exp(x)
