import numpy as np

from .model import Model


class Polynomial(Model):
    def __init__(self, degree=2):
        self.degree = degree

    def modelize(self, x):
        return [pow(xn, i) for xn in x for i in range(1, self.degree + 1)]

    def plot(self, x, theta):
        return theta[0] + sum(theta[i] * pow(x, i) for i in range(1, self.degree + 1))
