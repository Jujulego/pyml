from abc import ABC, abstractmethod


class Model(ABC):
    @abstractmethod
    def modelize(self, x):
        pass

    @abstractmethod
    def plot(self, x, theta):
        pass
