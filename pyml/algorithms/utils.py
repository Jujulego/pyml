from functools import wraps


# Utils
def hyperparameters(**hparams):
    """
    Adds hyperparameters to a algorithm.
    """

    def decorator(cls):
        oinit = cls.__init__

        @wraps(cls.__init__)
        def init(self, *args, **kwargs):
            for n, v in hparams.items():
                setattr(self, n, kwargs.get(n, v))
                kwargs.pop(n, None)

            oinit(self, *args, **kwargs)

        cls.__init__ = init
        return cls

    return decorator
