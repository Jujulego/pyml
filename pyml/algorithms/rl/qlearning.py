from pyml.algorithms.utils import hyperparameters

from .actor import Actor
from .env import Environment
from .utils import algorithm


# Algorithm
@hyperparameters(alpha=0.5, gamma=0.9, epsilon=0.1)
@algorithm()
def QLearning(self, env: Environment, actor: Actor):
    # Initialize Q
    Q = {
        (s, a): 0 for s in env.states() for a in actor.actions()
    }

    # Search optimal Q
    while True:
        pQ = Q
        Q = {}

        for s in env.states():
            for a in actor.actions():
                t = env.next(s, a)

                Q[s, a] = (1 - self.alpha) * pQ[s, a]
                Q[s, a] += self.alpha * (env.reward(s) + self.gamma * max(pQ[t, b] for b in actor.actions()))

        # Convergence test
        diff = sum(abs(Q[s, a] - pQ[s, a]) for s in env.states() for a in actor.actions())
        if diff < self.epsilon:
            break

    return self._compute_V_pi(env, actor, Q)
