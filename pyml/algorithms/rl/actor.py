from abc import ABC, abstractmethod

from .env import Environment


# Class
class Actor(ABC):
    def __init__(self, env: Environment):
        self.env = env

    # Methods
    @abstractmethod
    def actions(self):
        """
        Returns all possible actions

        :returns a list of actions
        """

        raise NotImplementedError

    @abstractmethod
    def probability(self, state, action, target):
        """
        Give the probability to reach state {target} from state {state} doing action {action}

        :param state: current state
        :param action: action to apply
        :param target: target state
        :returns a probability
        """

        raise NotImplementedError
