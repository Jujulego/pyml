from abc import ABC, abstractmethod


# Class
class Environment(ABC):
    # Methods
    @abstractmethod
    def states(self):
        """
        Returns all possible states

        :returns a list of states
        """

        raise NotImplementedError

    @abstractmethod
    def reward(self, state):
        """
        Returns the reward for the given state

        :param state: current state
        :returns the reward
        """

        raise NotImplementedError

    @abstractmethod
    def next(self, state, action):
        """
        Returns the following state considering the current state and a action

        :param state: current state
        :param action: an action
        :return: following state
        """

        raise NotImplementedError
