from pyml.algorithms.utils import hyperparameters

import numpy as np
import random

from .actor import Actor
from .env import Environment
from .utils import algorithm


# Algorithm
@hyperparameters(gamma=0.9)
@algorithm()
def PolicyIteration(self, env: Environment, actor: Actor):
    # Initialize
    N = len(env.states())

    pi = {s: random.choice(actor.actions()) for s in env.states()}
    R = np.array([env.reward(s) for s in env.states()])
    I = np.identity(N)

    # Search optimal pi
    while True:
        # Compute v
        A = self._compute_A(env, actor, pi)
        V = np.linalg.inv(I - self.gamma * A) @ R
        V = {env.states()[i]: V[i] for i in range(N)}

        # Compute pi
        ppi = pi
        pi = self._compute_pi(env, actor, V)

        # Convergence test
        if ppi == pi:
            break

    return V, pi
