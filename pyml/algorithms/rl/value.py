from pyml.algorithms.utils import hyperparameters

from .actor import Actor
from .env import Environment
from .utils import algorithm


# Algorithm
@hyperparameters(gamma=0.9, epsilon=0.1)
@algorithm()
def ValueIteration(self, env: Environment, actor: Actor):
    # Initialize V
    V = {
        s: 0 for s in env.states()
    }

    # Search optimal V
    while True:
        pV = V
        V = {}

        for s in env.states():
            V[s] = env.reward(s) + self.gamma * max(
                sum(actor.probability(s, a, t) * pV[t] for t in env.states())
                for a in actor.actions()
            )

        # Convergence test
        norm = sum(abs(V[s] - pV[s]) for s in env.states())
        if norm < self.epsilon:
            break

    return V, self._compute_pi(env, actor, V)
