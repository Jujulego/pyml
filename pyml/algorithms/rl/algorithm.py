import numpy as np

from abc import ABC, abstractmethod
from pyml.utils import argmax

from .actor import Actor
from .env import Environment


# Class
class Algorithm(ABC):
    # Methods
    def __init__(self):
        self.V = {}
        self.pi = {}

    @abstractmethod
    def _compute(self, env: Environment, actor: Actor):
        raise NotImplementedError

    @staticmethod
    def _compute_V_pi(env: Environment, actor: Actor, Q: dict):
        V = {}
        pi = {}

        for s in env.states():
            V[s] = -np.inf
            pi[s] = None

            for a in actor.actions():
                if Q[s, a] > V[s]:
                    V[s] = Q[s, a]
                    pi[s] = a

        return V, pi

    @staticmethod
    def _compute_pi(env: Environment, actor: Actor, V: dict):
        pi = {}

        for s in env.states():
            pi[s] = argmax({
                a: sum(
                    actor.probability(s, a, t) * V[t]
                    for t in env.states()
                )
                for a in actor.actions()
            })

        return pi

    @staticmethod
    def _compute_A(env: Environment, actor: Actor, pi):
        N = len(env.states())
        A = np.zeros((N, N))

        for i in range(N):
            s = env.states()[i]

            for j in range(N):
                t = env.states()[j]

                A[i, j] = actor.probability(s, pi[s], t)

        return A

    def compute(self, env: Environment, actor: Actor):
        self.V, self.pi = self._compute(env, actor)
