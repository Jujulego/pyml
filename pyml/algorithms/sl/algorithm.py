from abc import ABC, abstractmethod
from collections import namedtuple

import numpy as np

from pyml.models.model import Model
from pyml.models.linear import Linear


class Algorithm(ABC):
    TestResults = namedtuple('TestResults', ('inputs', 'outputs', 'results', 'sse', 'mse'))

    def __init__(self, model: Model = Linear()):
        self.theta = None
        self.model = model

    @abstractmethod
    def _train(self, inputs, outputs):
        pass

    def _compute(self, x):
        return self.theta.T @ x

    def _prepare(self, x):
        return np.array([1.0, *self.model.modelize(x)])

    def train(self, inputs, outputs):
        assert len(inputs) == len(outputs)

        inputs = [self._prepare(x) for x in inputs]
        self.theta = self._train(inputs, outputs)

    def compute(self, x):
        assert self.theta is not None

        return self._compute(self._prepare(x))

    def test(self, inputs, outputs) -> TestResults:
        assert len(inputs) == len(outputs)

        results = []
        sse = 0

        for x, y in zip(inputs, outputs):
            r = self.compute(x)

            sse += pow(np.linalg.norm(r - y), 2)
            results.append(r)

        return self.TestResults(
            inputs, outputs, results,
            sse/2, sse/len(inputs)
        )

    def plot_model(self, x):
        return self.model.plot(x, self.theta)
