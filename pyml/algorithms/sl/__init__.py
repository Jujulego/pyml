from .bgd import BatchGradientDescent
from .cfs import CloseFormSolution
from .ffnn import FeedForwardNeuralNetwork, FeedForwardNeuralLayer
from .ffnn2l import FeedForwardNeuralNetwork2L
from .ridge import RIDGE
from .sgd import StochasticGradientDescent

BGD = BatchGradientDescent
CFS = CloseFormSolution
SGD = StochasticGradientDescent

FFNL = FeedForwardNeuralLayer
FFNN = FeedForwardNeuralNetwork
FFNN2L = FeedForwardNeuralNetwork2L
