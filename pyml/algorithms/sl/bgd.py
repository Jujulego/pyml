from pyml.algorithms.utils import hyperparameters

import numpy as np

from .utils import algorithm


@hyperparameters(alpha=0.01, epsilon=0.00001)
@algorithm()
def BatchGradientDescent(algo, inputs, outputs):
    deg = len(inputs[0])
    theta = np.random.rand(deg)

    while True:
        gradient = sum(
            ((theta.T @ x) - y) * x
            for x, y in zip(inputs, outputs)
        )
        theta -= algo.alpha * gradient

        if np.linalg.norm(gradient) < algo.epsilon:
            break

    return theta
