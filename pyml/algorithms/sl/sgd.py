from pyml.algorithms.utils import hyperparameters
from random import randrange

import numpy as np

from .utils import algorithm


# Algorithm
@hyperparameters(alpha=0.01, count=1000)
@algorithm()
def StochasticGradientDescent(algo, inputs, outputs):
    deg = len(inputs[0])
    theta = np.random.rand(deg)

    for _ in range(algo.count):
        i = randrange(len(inputs))
        x, y = inputs[i], outputs[i]

        theta -= algo.alpha * ((theta.T @ x) - y) * x

    return theta
