from pyml.models import Linear
from pyml.algorithms.utils import hyperparameters

import numpy as np

from .algorithm import Algorithm


# Classes
class FeedForwardNeuralLayer:
    def __init__(self, size, *, alpha=0.1):
        self.size = size
        self.alpha = alpha
        self.matrix = None
        self.gradient = None

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def compute(self, xb):
        assert self.matrix is not None
        return self.sigmoid(xb.T @ self.matrix)

    def init(self, n):
        self.matrix = np.random.rand(n, self.size)

    def init_gradient(self):
        self.gradient = np.zeros(self.matrix.shape)

    def update_gradient(self, xb, g, err):
        xb = xb.reshape((len(xb), 1))
        v = (err * g * (1 - g)).reshape((1, self.size))

        self.gradient += xb @ v

        return v

    def apply_gradient(self):
        self.matrix -= self.alpha * self.gradient


@hyperparameters(alpha=0.1, epsilon=0.01)
class FeedForwardNeuralNetwork(Algorithm):
    def __init__(self, *layers: FeedForwardNeuralLayer):
        super().__init__(model=Linear())

        self.layers = list(layers)

    def _layers(self, xb):
        result = xb[1:]

        for l in self.layers:
            result = l.compute(np.array([1.0, *result]))
            yield result

    def _compute(self, xb):
        result = xb[1:]

        for l in self.layers:
            result = l.compute(np.array([1.0, *result]))

        return result

    def _train(self, inputs, outputs):
        n = len(inputs[0])   # size of input
        j = len(outputs[0])  # size of output

        # add output layer
        self.layers.append(
            FeedForwardNeuralLayer(j, alpha=self.alpha)
        )

        # init layers matrices
        for l in self.layers:
            l.init(n)
            n = l.size + 1  # the input of each layer is the output of the previous

        # train network !
        while True:
            # init gradients
            for l in self.layers:
                l.init_gradient()

            # compute gradients
            for xb, y in zip(inputs, outputs):
                results = [xb[1:], *self._layers(xb)]
                err = results[-1] - y

                results.reverse()
                for l, x, g in zip(reversed(self.layers), results[1:], results[:-1]):
                    v = l.update_gradient(np.array([1.0, *x]), g, err)
                    err = (v * l.matrix[1:]).sum(axis=1)

            # apply gradients
            for l in self.layers:
                l.apply_gradient()

            # test for convergency
            if all(np.linalg.norm(l.gradient) < self.epsilon for l in self.layers):
                break

        return [l.matrix for l in self.layers]
