import numpy as np

from .utils import algorithm


# Algorithm
@algorithm()
def CloseFormSolution(_, inputs, outputs):
    X = np.array(inputs)
    Y = np.array(outputs).reshape((len(outputs), 1))

    return np.linalg.inv(X.T @ X) @ (X.T @ Y)
