from .algorithm import Algorithm


def algorithm(name=None, base=Algorithm):
    """
    Creates an algorithm around a train function
    """

    def decorator(train):
        class A(base):
            def _train(self, inputs, outputs):
                return train(self, inputs, outputs)

        A.__qualname__ = name or train.__name__
        return A

    return decorator
