from pyml.algorithms.utils import hyperparameters

import numpy as np

from .utils import algorithm


# Algorithm
@hyperparameters(lambda_=1)
@algorithm()
def RIDGE(algo, inputs, outputs):
    X = np.array(inputs)
    Y = np.array(outputs).reshape((len(outputs), 1))

    I = np.identity(len(inputs[0]))
    I[0, 0] = 0

    return np.linalg.inv(X.T @ X + algo.lambda_ * I) @ (X.T @ Y)
