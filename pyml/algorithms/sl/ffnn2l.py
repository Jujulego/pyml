import numpy as np

from .algorithm import Algorithm
from pyml.algorithms.utils import hyperparameters


# Classes
@hyperparameters(k=5, alpha_v=0.1, alpha_w=0.1, epsilon=0.01)
class FeedForwardNeuralNetwork2L(Algorithm):
    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def _layers(self, theta, xb):
        V, W = theta

        f = self.sigmoid(xb.T @ V)
        fb = np.array([1.0, *f])

        g = self.sigmoid(fb.T @ W)

        return f, g

    def _compute(self, x):
        return self._layers(self.theta, x)[1]

    def _train(self, inputs, outputs):
        N = len(inputs[0]) - 1
        K = self.k
        J = len(outputs[0])

        V = np.random.rand(N+1, K)
        W = np.random.rand(K+1, J)

        # Compute !
        while True:
            gradient_v = np.zeros((N+1, K))
            gradient_w = np.zeros((K+1, J))

            for xb, y in zip(inputs, outputs):
                f, g = self._layers([V, W], xb)

                xb = xb.reshape((N+1, 1))
                fb = np.array([1.0, *f]).reshape((K+1, 1))

                vw = (g - y) * g * (1 - g)
                vv = (vw * W[1:]).sum(axis=1) * f * (1 - f)

                gradient_w += fb @ vw.reshape((1, J))
                gradient_v += xb @ vv.reshape((1, K))

            W -= self.alpha_w * gradient_w
            V -= self.alpha_v * gradient_v

            norm_v = np.linalg.norm(gradient_v)
            norm_w = np.linalg.norm(gradient_w)
            if norm_v < self.epsilon and norm_w < self.epsilon:
                break

        return V, W
