import math


# Utils
def argmax(values: dict):
    val_max = -math.inf
    arg_max = None

    for arg, val in values.items():
        if val > val_max:
            val_max = val
            arg_max = arg

    return arg_max


def load(filename):
    inputs = []
    outputs = []

    with open(filename) as f:
        for l in f.readlines():
            values = l.strip().split(' ')

            inputs.append([float(v) for v in values[:-1]])
            outputs.append(float(values[-1]))

    return inputs, outputs
