from matplotlib import pyplot as plt
import numpy as np

from pyml.algorithms import BGD, SGD, CFS
from pyml.utils import load


def test(algo, inputs, outputs):
    results = algo.test(inputs, outputs)

    print(algo.theta)
    print("SSE: %f" % results.sse)
    print("MSE: %f" % results.mse)


if __name__ == '__main__':
    x = np.arange(0, 2, 0.01)

    # load data
    inputs, outputs = load("data.txt")

    plt.plot(inputs[:70], outputs[:70], label="train data")
    plt.plot(inputs[70:], outputs[70:], label="test data")

    # BGD
    bgd = BGD()
    bgd.train(inputs[:70], outputs[:70])

    plt.plot(x, bgd.plot_model(x), label="bgd")

    # SGD
    sgd = SGD()
    sgd.train(inputs[:70], outputs[:70])

    plt.plot(x, sgd.plot_model(x), label="sgd")

    # CFS
    cfs = CFS()
    cfs.train(inputs[:70], outputs[:70])
    test(cfs, inputs[70:], outputs[70:])

    plt.plot(x, cfs.plot_model(x), label="cfs")

    # Plot !
    plt.legend()
    plt.show()
