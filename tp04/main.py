from pprint import pprint
from pyml.algorithms.rl import PolicyIteration, QLearning, ValueIteration

from map import Map
from robot import Robot


if __name__ == '__main__':
    # Initialization
    map = Map()
    robot = Robot(map)

    # Value iteration
    value = ValueIteration(gamma=0.99)
    value.compute(map, robot)

    pprint(value.V)
    pprint(value.pi)

    # Policy iteration
    # policy = PolicyIteration(gamma=0.99)
    # policy.compute(map, robot)
    #
    # pprint(policy.V)
    # pprint(policy.pi)

    # QLearning
    qlearn = QLearning(alpha=0.5, gamma=0.99)
    qlearn.compute(map, robot)

    pprint(qlearn.V)
    pprint(qlearn.pi)
