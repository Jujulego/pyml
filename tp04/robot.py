import numpy as np

from enum import Enum, unique
from pyml.algorithms.rl import Actor

from map import STATES, HELL, HEAVEN


# Enums
@unique
class Action(Enum):
    N = (0, 1)
    S = (0, -1)
    E = (1, 0)
    W = (-1, 0)


ERRORS = {
    Action.N: [Action.E, Action.W],
    Action.S: [Action.E, Action.W],
    Action.E: [Action.N, Action.S],
    Action.W: [Action.N, Action.S]
}


# Class
class Robot(Actor):
    # Methods
    def actions(self):
        return tuple(Action)

    def probability(self, state, action, target):
        if state in [HELL, HEAVEN]:
            return 1 if target == state else 0

        proba = 0
        if np.array_equal(target, self.env.next(state, action)):
            proba += 0.8

        proba += sum(
            0.1 if np.array_equal(target, self.env.next(state, a)) else 0
            for a in ERRORS[action]
        )

        return proba
