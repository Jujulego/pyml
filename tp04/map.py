import numpy as np

from pyml.algorithms.rl import Environment

# Constants
HEAVEN = (4, 3)
HELL = (4, 2)
STATES = [
    (1, 1),
    (1, 2),
    (1, 3),
    (2, 1),
    (2, 3),
    (3, 1),
    (3, 2),
    (3, 3),
    (4, 1),
    HELL,
    HEAVEN
]

DEFAULT_REWARD = -0.02
REWARDS = {
    HEAVEN: 1,
    HELL: -1
}


# Class
class Map(Environment):
    # Methods
    def states(self):
        return STATES

    def reward(self, state):
        return REWARDS.get(state, DEFAULT_REWARD)

    def next(self, state, action):
        if state in [HELL, HEAVEN]:
            return state

        n = tuple(np.sum([state, action.value], axis=0))
        return n if any(np.array_equal(n, s) for s in STATES) else state
