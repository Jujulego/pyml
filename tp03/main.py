from datetime import datetime

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

from pyml.algorithms import FFNN2L

from pyml.utils import load


def color(p):
    if p > 0.75:
        color = 32  # vert
    elif p < 0.25:
        color = 31  # rouge
    else:
        color = 33  # jaune

    return "\033[{}m{: .2f} %\033[m".format(color, p * 100)


def plot3d(ax, inputs, outputs, **kwargs):
    xs = []
    ys = []
    zs = []

    for x, y in zip(inputs, outputs):
        xs.append(x[0])
        ys.append(x[1])
        zs.append(y[1])

    ax.scatter(np.array(xs), np.array(ys), np.array(zs), **kwargs)


if __name__ == '__main__':
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # load data
    inputs, outputs = load("data.txt")
    outputs = [[1-y, y] for y in outputs]

    plot3d(ax, inputs, outputs, label="dataset")

    # FFNN2L
    ffnn2l = FFNN2L(alpha_v=0.5, alpha_w=0.125, epsilon=0.05)

    start = datetime.now()
    ffnn2l.train(inputs, outputs)
    end = datetime.now()

    print(ffnn2l.theta[0])
    print(ffnn2l.theta[1])

    result = ffnn2l.test(inputs, outputs)
    for x, y, g in zip(inputs, outputs, result.results):
        print(np.around(x, decimals=3), y, color(g[0]), color(g[1]), sep="\t")

    plot3d(ax, inputs, result.results, label="Test (train)")

    print("Compute time:", (end - start).total_seconds() * 1000, "ms")
    print("SSE:", result.sse)
    print("MSE:", result.mse)

    testx = np.array([[2, 2], [4, 4]])
    testg = []
    for x in testx:
        g = ffnn2l.compute(x)

        testg.append(g)
        print(np.around(x, decimals=3), color(g[0]), color(g[1]), sep="\t")

    plot3d(ax, testx, testg, label="Test")
    plt.show()
